package com.darryring.fast.util;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by hljdrl on 16/6/2.
 */
public class InputUtil {

    public  static void hideInput(Context ctx)
    {
        Context _contect = ctx.getApplicationContext();
        InputMethodManager imm = (InputMethodManager)_contect.getSystemService(Context.INPUT_METHOD_SERVICE);
        //得到InputMethodManager的实例
        if (imm.isActive()) {
            //如果开启
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_NOT_ALWAYS);
            //关闭软键盘，开启方法相同，这个方法是切换开启与关闭状态的
        }
    }
}
