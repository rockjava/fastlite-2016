package com.darryring.fast.fragment;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.darryring.fast.R;
import com.darryring.fast.adapter.SessionAdapter;
import com.darryring.fast.util.DrawableUtil;
import com.darryring.fast.util.MoviceSize;
import com.darryring.libmodel.entity.ChatID;
import com.darryring.libmodel.entity.FSession;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hljdrl on 16/3/2.
 */
public class SessionFragment extends BaseFragment implements AdapterView.OnItemClickListener{


    ListView mListview;
    SessionAdapter mAdapter;
    List<FSession> datalist = new ArrayList<FSession>();
    public SessionFragment(){
    }
    @SuppressLint("ValidFragment")
    public SessionFragment(String _title){
        setTitle(_title);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View _view = inflater.inflate(R.layout.fragment_session,container,false);
        return _view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View _view = getView();
        mListview = (ListView) _view.findViewById(R.id.listview_session);
        MoviceSize _size =  MoviceSize.makeHeight(500,600);
        //---------------------------------------------------------------------
        LinearLayout _mHeaderViewLayout = new LinearLayout(getActivity());
        LinearLayout.LayoutParams _params = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, _size.height);
        //--------------------------------------------------------------------
        View _hearderView = View.inflate(getActivity(),R.layout.content_session_header,null);
        _mHeaderViewLayout.addView(_hearderView,_params);
        mListview.addHeaderView(_mHeaderViewLayout);
        //==================================================================
        datalist.addAll(data());

        mAdapter = new SessionAdapter(getActivity(),datalist);
        //===================================================================
        TypedArray ta = getActivity().getTheme().obtainStyledAttributes(R.styleable.FastColorTheme);
        int _defaultColor = Color.TRANSPARENT;
        int _selectColor = ta.getColor(R.styleable.FastColorTheme_colorFastNomal,0);
        ColorStateList colorStateList = DrawableUtil.createTabBarColorStateList(_defaultColor,_selectColor);
        Drawable _drawable = mListview.getSelector();
        Drawable _tintDrawable =  DrawableUtil.tintDrawable(_drawable,colorStateList);
        mListview.setSelector(_tintDrawable);
        mListview.setAdapter(mAdapter);
        //----
        ta.recycle();

    }
    List<FSession> data(){
        List<FSession> _copy = new ArrayList<>();

        FSession _f1 = new FSession();
        _f1.setFromId(ChatID.CHAT_ID_SYSTEM_100);
        _f1.setMessage("洗洗");
        _copy.add(_f1);
        //
        FSession _f2 = new FSession();
        _f2.setFromId(ChatID.CHAT_ID_SYSTEM_200);
        _f2.setMessage("大大");
        _copy.add(_f2);

        FSession _f3 = new FSession();
        _f3.setFromId(ChatID.CHAT_ID_SYSTEM_300);
        _f3.setMessage("什么");
        _copy.add(_f3);

        FSession _f4 = new FSession();
        _f4.setFromId(ChatID.CHAT_ID_SYSTEM_400);
        _f4.setMessage("韩剧..");
        _copy.add(_f4);

        FSession _f5 = new FSession();
        _f5.setFromId(ChatID.CHAT_ID_SYSTEM_500);
        _f5.setMessage("通知");
        _copy.add(_f5);

        FSession _f6 = new FSession();
        _f6.setFromId(ChatID.CHAT_ID_SYSTEM_600);
        _f6.setMessage("六月一");
        _copy.add(_f6);

        FSession _f7 = new FSession();
        _f7.setFromId(ChatID.CHAT_ID_SYSTEM_700);
        _f7.setMessage("明天去哪里");
        _copy.add(_f7);

        FSession _f8 = new FSession();
        _f8.setFromId(ChatID.CHAT_ID_SYSTEM_800);
        _f8.setMessage("后天计划");
        _copy.add(_f8);

        FSession _f9 = new FSession();
        _f9.setFromId(ChatID.CHAT_ID_SYSTEM_900);
        _f9.setMessage("手机号");
        _copy.add(_f9);


        return _copy;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            String _select = (String) adapterView.getAdapter().getItem(i);
    }
}
