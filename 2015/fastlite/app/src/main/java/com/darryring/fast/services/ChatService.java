package com.darryring.fast.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.darryring.libcore.Fast;
import com.darryring.libcore.android.logger.AndroidLogger;

/**
 * Created by hljdrl on 16/3/28.
 */
public class ChatService extends Service {
    private String TAG="ChatService";
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        if(Fast.logger==null){
            Fast.logger = new AndroidLogger(true,true,true,true);
        }
        Fast.logger.i(TAG,"onCreate...");
//        if(LibChat.chatClient==null) {
//            LibChat.chatClient = new EaseMobClient();
//            LibChat.chatClient.init(getApplicationContext());
//        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Fast.logger.i(TAG,"onStartCommand...");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Fast.logger.i(TAG,"onStart...");
        super.onStart(intent, startId);
    }


    @Override
    public void onDestroy() {
        Fast.logger.i(TAG,"onDestroy...");
        super.onDestroy();
    }
}
