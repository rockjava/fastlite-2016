package com.darryring.fast.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.darryring.fast.R;

import java.util.List;

/**
 * Created by hljdrl on 16/5/21.
 */
public class BrowserAdapter extends ArrayAdapter<String> {

    LayoutInflater inflater;
    public BrowserAdapter(Context context, List<String> objects) {
        super(context, 0, objects);
        inflater = LayoutInflater.from(context);
    }


    @Override
    public View getView(int position, View _view, ViewGroup parent) {
        ViewTag _tag = null;
        if(_view==null){
            _view = inflater.inflate(R.layout.item_browser_uri,null);
            _tag = new ViewTag();
            _tag.textview = (TextView) _view.findViewById(R.id.tv_uri);
            _view.setTag(_tag);
        }else{
            _tag = (ViewTag) _view.getTag();
        }
        _tag.textview.setText(getItem(position));
        return _view;
    }
    private static class ViewTag{
        public TextView textview;
    }
}
