package com.darryring.libcore.android.cache;

import com.darryring.libcore.cache.KVCache;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hljdrl on 15/12/26.
 */
public class MemoryCache implements KVCache {


    /**
     *
     */
    Map<String,String> mMap;

    /**
     *
     */
    Map<String,Object> memoryMap = new HashMap<String,Object>();

    /**
     *
     */
    public MemoryCache(){
        mMap = new HashMap<String,String>();
    }

    /**
     * @param k
     * @param v
     */
    @Override
    public void saveToFileCache(String k, String v) {
        mMap.put(k,v);
    }

    /**
     * @param k
     * @param obj
     */
    @Override
    public void saveToMemory(String k, Object obj) {
        memoryMap.put(k,obj);
    }

    /**
     * @param k
     * @return
     */
    @Override
    public Object readFromMemory(String k) {
        return memoryMap.get(k);
    }

    /**
     * @param k
     */
    @Override
    public void deleteToMemory(String k) {
        memoryMap.remove(k);
    }
    /**
     * @param k
     * @return
     */
    @Override
    public String readFromFileCache(String k) {
        return mMap.get(k);
    }

    /**
     * @param k
     */
    @Override
    public void deleteToFileCache(String k) {
        mMap.remove(k);
    }
}
