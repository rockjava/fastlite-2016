package com.darryring.libchat.call;

/**
 * Created by hljdrl on 15/11/18.
 */
public interface MessageCall<T> {

    public void onProgress(int progress, String msgId);

    public void onSuccess(T msg);

    public void onError(T msg);


}
