package com.libtrace.imageselector.adapter;

import com.alexvasilkov.gestures.views.interfaces.GestureView;

public interface GestureSettingsSetupListener {
    void onSetupGestureView(GestureView view);
}
