package com.libtrace.imageselector.utils;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 文件操作类
 */
public class FileUtils {

    /**
     *  "file:///mnt/sdcard/image.png" // from SD card
     * 格式化File路径,ImageLoader要求格式
     * @param file
     * @return
     */
    public static String foramtUriFile(String file){
        StringBuffer _buf = new StringBuffer();
        _buf.append("file://").append(file);
        return _buf.toString();
    }

    /**
     * "http://site.com/image.png" // from Web
     * @param http
     * @return
     */
    public static String formatUriHttp(String http){
        return String.valueOf(http);
    }

    /**
     * "assets://image.png"
     * @param assets
     * @return
     */
    public static String formatUriAssets(String assets){
        StringBuffer _buf = new StringBuffer();
        _buf.append("assets://").append(assets);
        return _buf.toString();
    }

    /**
     * "drawable://" + R.drawable.img // from drawables (non-9patch images)
     * @param drawable
     * @return
     */
    public static String formatUriDrawable(int drawable){
        StringBuffer _buf = new StringBuffer();
        _buf.append("drawable://").append(drawable);
        return _buf.toString();
    }



    public static File createTmpFile(Context context){

        String state = Environment.getExternalStorageState();
        if(state.equals(Environment.MEDIA_MOUNTED)){
            // 已挂载
            File pic = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CHINA).format(new Date());
            String fileName = "photo_camera_image_"+timeStamp+"";
            File tmpFile = new File(pic, fileName+".jpg");
            return tmpFile;
        }else{
            File cacheDir = context.getCacheDir();
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CHINA).format(new Date());
            String fileName = "photo_camera_"+timeStamp+"";
            File tmpFile = new File(cacheDir, fileName+".jpg");
            return tmpFile;
        }

    }

}
