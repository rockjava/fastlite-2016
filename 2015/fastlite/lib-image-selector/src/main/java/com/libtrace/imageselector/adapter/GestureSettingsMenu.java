package com.libtrace.imageselector.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;

import com.alexvasilkov.gestures.GestureController;
import com.alexvasilkov.gestures.Settings;
import com.alexvasilkov.gestures.views.interfaces.GestureView;

/**
 * Created by hljdrl on 16/1/6.
 */
public class GestureSettingsMenu implements GestureSettingsSetupListener {

    private boolean mIsPanEnabled = true;
    private boolean mIsZoomEnabled = true;
    private boolean mIsRotationEnabled = false;
    private boolean mIsOverscrollXEnabled = true;
    private boolean mIsOverscrollYEnabled = true;
    private boolean mIsOverzoomEnabled = true;
    private boolean mIsFitViewport = true;
    private Settings.Fit mFitMethod = Settings.Fit.INSIDE;
    private int mGravity = Gravity.CENTER;
    private Activity mActivity;
    public GestureSettingsMenu(Activity _activity){
        mActivity = _activity;
    }

    @Override
    public void onSetupGestureView(GestureView view) {
        Context context = ((View) view).getContext();
        float overscrollX = mIsOverscrollXEnabled ? 32f : 0f;
        float overscrollY = mIsOverscrollYEnabled ? 32f : 0f;
        float overzoom = mIsOverzoomEnabled ? Settings.OVERZOOM_FACTOR : 1f;

        view.getController().getSettings()
                .setPanEnabled(mIsPanEnabled)
                .setZoomEnabled(mIsZoomEnabled)
                .setDoubleTapEnabled(mIsZoomEnabled)
                .setOverscrollDistance(context, overscrollX, overscrollY)
                .setOverzoomFactor(overzoom)
                .setMaxZoom(overzoom*3)
                .setRotationEnabled(mIsRotationEnabled)
                .setFillViewport(mIsFitViewport)
                .setFitMethod(mFitMethod)
                .setGravity(mGravity);
        view.getController().setOnGesturesListener(new GestureController.OnGestureListener() {
            @Override
            public void onDown(MotionEvent e) {
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return false;
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                if(mActivity!=null){
                    mActivity.finish();
                }
                return true;

            }

            @Override
            public void onLongPress(MotionEvent e) {

            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                return false;
            }
        });

    }
    private enum GravityType {
        CENTER(Gravity.CENTER),
        TOP(Gravity.TOP),
        BOTTOM(Gravity.BOTTOM),
        START(Gravity.START),
        END(Gravity.END),
        TOP_START(Gravity.TOP | Gravity.START),
        BOTTOM_END(Gravity.BOTTOM | Gravity.END);

        public final int gravity;

        GravityType(int gravity) {
            this.gravity = gravity;
        }

        public static GravityType find(int gravity) {
            for (GravityType type : values()) {
                if (type.gravity == gravity) return type;
            }
            return null;
        }
    }
}
